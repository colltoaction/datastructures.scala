sealed abstract class BaseTree {
	def IsEmpty = {
		this match {
			case t: EmptyTree => true
			case _ => false
		}
	}

	def Count : Int = {
		this match {
			case EmptyTree() => 0
			case TreeNode(v, l, r) => 1 + l.Count + r.Count
		}
	}
}

case class EmptyTree() extends BaseTree
case class TreeNode(v: Object, l: BaseTree, r: BaseTree) extends BaseTree


val tree = TreeNode(	
	"hola",
	TreeNode(	
		"hola vite",
		EmptyTree(),
		EmptyTree()),
	EmptyTree()
	)
println(tree.Count)
